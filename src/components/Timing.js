import React, { useInsertionEffect, useState } from 'react'

function Timing() {
    const [clockState, setClockState] = useState();
    useInsertionEffect(() => {
        setInterval(() => {
            const date = new Date();
            setClockState(date.toLocaleTimeString());
        }, 1000);
    }, [])
    
    return (
        <div>
            <h1 style={{ margin: "5px" }}>Hello, world!</h1>
            <p style={{ margin: "5px" }}>It is {clockState}</p>
        </div>
    )
}

export default Timing

