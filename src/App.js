import Timing from "./components/Timing";
import React, {useState} from "react";

function App() {
  const [timeDays, setTimeDays] = useState();
  const [timeHours, setTimeHours] = useState();
  const [timeMinutes, settimeMinutes] = useState();
  const [timeSeconds, setTimerSeconds] = useState();
  return (
    <div>
      <Timing timeDays={timeDays} timeHours={timeHours} timeMinutes={timeMinutes} timeSeconds={timeSeconds}/>
    </div>
  );
}

export default App;
